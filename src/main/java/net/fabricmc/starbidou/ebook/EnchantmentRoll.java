package net.fabricmc.starbidou.ebook;

public class EnchantmentRoll {

    public final String enchantmentIdentifier;
    public final float chance;

    public EnchantmentRoll(String enchantmentIdentifier, float chance)
    {
        this.enchantmentIdentifier = enchantmentIdentifier;
        this.chance = chance;
    }
}
