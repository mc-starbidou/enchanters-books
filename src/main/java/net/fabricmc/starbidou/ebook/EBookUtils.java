package net.fabricmc.starbidou.ebook;

import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtHelper;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class EBookUtils {

    private static final String NBT_STARBIDOU = "starbidou";
    private static final String NBT_EBOOK = "ebook";
    private static final String NBT_ROLLS = "rolls";
    private static final String NBT_ID = "id";
    private static final String NBT_CHANCE = "chance";

    public static List<EnchantmentRoll> GetEnchantmentRolls(ItemStack itemStack)
    {
        var list = new ArrayList<EnchantmentRoll>();

        var root = itemStack.getNbt();
        if( root != null )
        {
            if(root.contains("tag", NbtElement.COMPOUND_TYPE))
            {
                var tag = root.getCompound("tag");
                if( tag.contains(NBT_STARBIDOU, NbtElement.COMPOUND_TYPE))
                {
                    var starbidou = tag.getCompound(NBT_STARBIDOU);
                    if(starbidou.contains(NBT_EBOOK, NbtElement.COMPOUND_TYPE))
                    {
                        var ebook = starbidou.getCompound(NBT_EBOOK);
                        if(ebook.contains(NBT_ROLLS, NbtElement.LIST_TYPE))
                        {
                            var rolls = ebook.getList(NBT_ROLLS, NbtElement.COMPOUND_TYPE);
                            for(var rollElement : rolls)
                            {
                                if( rollElement instanceof NbtCompound)
                                {
                                    try{
                                        var roll = (NbtCompound)rollElement;

                                        if( roll.contains(NBT_ID, NbtElement.STRING_TYPE))
                                        {
                                            var id = roll.getString(NBT_ID);
                                            var chance = roll.getFloat(NBT_CHANCE);

                                            list.add(new EnchantmentRoll(id, chance));
                                        }
                                    }
                                    catch (ClassCastException e)
                                    {
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return list;
    }

    public static List<EnchantmentRoll> MergeRolls(List<EnchantmentRoll> a, List<EnchantmentRoll> b)
    {
        List<EnchantmentRoll> result = new ArrayList<>();

        java.util.function.Consumer<List<EnchantmentRoll>> func = (list) -> {
            for (EnchantmentRoll roll : list ) {

                int i = 0;

                for(; i < result.size(); ++i)
                {
                    EnchantmentRoll r = result.get(i);

                    if(r.enchantmentIdentifier.compareTo(roll.enchantmentIdentifier) == 0)
                    {
                        double base = r.chance;
                        double additional = roll.chance;

                        double merged = base + Math.max(0.01, (1.0 - base) * additional);
                        merged = Math.min(Math.max(merged, 0), 1);

                        result.set(i, new EnchantmentRoll(r.enchantmentIdentifier, (float)merged));
                        break;
                    }
                }

                if( i >= result.size())
                {
                    result.add(roll);
                }
            }
        };

        func.accept(a);
        func.accept(b);

        return result;
    }

    public static ItemStack generateEBook(EnchantmentRoll... rolls)
    {
        var itemStack = new ItemStack(Registries.ITEM.get(EBookMod.ENCHANTED_BOOK_IDENTIFIER), 1);

        try {
            itemStack.setNbt(EBookUtils.generateEBookNbt(rolls));
        } catch (CommandSyntaxException e) {
            EBookMod.LOGGER.error("Exception raised while generating EBook NBT.", e);
            return ItemStack.EMPTY;
        }

        return  itemStack;
    }

    public static NbtCompound generateEBookNbt(EnchantmentRoll... rolls) throws CommandSyntaxException {

        List<String> rollsNbt = new ArrayList<>();
        List<String> rollsLoreNbt = new ArrayList<>();

        for(EnchantmentRoll roll : rolls)
        {
            var enchantment = Registries.ENCHANTMENT.get(new Identifier(roll.enchantmentIdentifier));

            float chance = Math.min(1f, Math.max(0f, roll.chance));
            String percent = getPercentChanceText(chance);
            String quantity = enchantment.getMaxLevel() <= 1 ? "" : "1";

            rollsNbt.add("{id:'" + roll.enchantmentIdentifier + "', chance: " + chance + "f}");
            rollsLoreNbt.add("'[{\"text\":\"+"+quantity+" \",\"color\":\"gray\",\"italic\":false},{\"translate\":\""+enchantment.getTranslationKey()+"\"},{\"text\":\" (\"},{\"text\":\""+percent+"\",\"color\":\"light_purple\"},{\"text\":\")\"}]'");
        }

        return NbtHelper.fromNbtProviderString("{tag:{starbidou:{ebook:{rolls:["+String.join(", ", rollsNbt)+"]}}}, display:{Name:'{\"translate\":\"item.starbidous_ebook.enchanters_book\",\"color\":\"aqua\",\"italic\":false}',Lore:["+String.join(", ", rollsLoreNbt)+"]}}");
    }

    public static String getPercentChanceText(float chance)
    {
        if( chance >= 0.1f )
        {
            return ((int)Math.round(chance * 100)) + "%";
        }
        else if( chance >= 0.01f)
        {
            return String.format("%.1f%%", Math.round(1000 * chance)/10f);
        }
        else
        {
            return String.format("%f%%", 100 * chance);
        }
    }

}
