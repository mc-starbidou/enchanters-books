package net.fabricmc.starbidou.ebook;

import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.loot.v2.LootTableEvents;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtHelper;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.random.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EBookMod implements ModInitializer {
	public static final Logger LOGGER = LoggerFactory.getLogger("starbidous_ebook");
	public static final Identifier ENCHANTED_BOOK_IDENTIFIER = new Identifier("minecraft", "enchanted_book");
	private static HashMap<Identifier, Float> enchantmentWeights = new HashMap<Identifier, Float>();
	private static HashMap<Identifier, Float> lootTableChances = new HashMap<Identifier, Float>();

	private static final float[] MIN_EBOOK_ACTIVATION_CHANCE = {0.25f, 0.31f, 0.37f, 0.43f, 0.50f};
	private static final float[] MAX_EBOOK_ACTIVATION_CHANCE = {0.45f, 0.52f, 0.59f, 0.67f, 0.75f};

	@Override
	public void onInitialize() {
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution.

		configureBuiltInEnchantments();
		configureBuiltInLootTables();

		LootTableEvents.MODIFY.register(((resourceManager, lootManager, id, tableBuilder, source) -> {
			if( lootTableChances.containsKey(id) )
			{
				var enchantedBook = Registries.ITEM.get(ENCHANTED_BOOK_IDENTIFIER);

				LootPool.Builder poolBuilder = LootPool.builder()
						.with(new ItemEntry(
								enchantedBook,
								1,
								0,
								new LootCondition[]{},
								new LootFunction[]{ new EBookLootFunction() }))
						.rolls(new EBookChanceProvider(id));

				tableBuilder.pool(poolBuilder);
			}
		}));
	}

	public static void configureEnchantment(String enchantmentIdentifier, float weight)
	{
		configureEnchantment(new Identifier(enchantmentIdentifier), weight);
	}
	public static void configureEnchantment(Identifier enchantmentIdentifier, float weight)
	{
		enchantmentWeights.put(enchantmentIdentifier, weight);
	}

	public static void configureLootTableRollChance(String lootTableIdentifier, float chance)
	{
		lootTableChances.put(new Identifier(lootTableIdentifier), chance);
	}
	public static void configureLootTableRollChance(Identifier lootTableIdentifier, float chance)
	{
		configureLootTableRollChance(lootTableIdentifier, chance);
	}

	public static float getLootTableRollChance(Identifier lootTableIdentifier)
	{
		return lootTableChances.getOrDefault(lootTableIdentifier, 0f);
	}

	public static Identifier getRandomEnchantmentIdentifier(Random random)
	{
		if( enchantmentWeights.size() == 0) return null;

		List<Identifier> list = new ArrayList<>();
		float totalWeight = 0;

		for(var identifier : enchantmentWeights.keySet())
		{
			list.add(identifier);
			totalWeight += enchantmentWeights.get(identifier);
		}

		float targetWeight = totalWeight * random.nextBetween(0, 10000000) / 10000000f;

		float sum = 0;
		for(var identifier : list)
		{
			sum += enchantmentWeights.get(identifier);
			if(sum >= targetWeight)
			{
				return identifier;
			}
		}

		return list.get(list.size() - 1);
	}

	public static ItemStack generateEBook(Random random)
	{
		var itemStack = new ItemStack(Registries.ITEM.get(ENCHANTED_BOOK_IDENTIFIER), 1);
		var identifier = getRandomEnchantmentIdentifier(random);
		var enchantment = Registries.ENCHANTMENT.get(identifier);
		var chance = generateEBookChance(enchantment, random);

		try {
			itemStack.setNbt(EBookUtils.generateEBookNbt(new EnchantmentRoll(identifier.toString(), chance)));
		} catch (CommandSyntaxException e) {
			return ItemStack.EMPTY;
		}

		return  itemStack;
	}

	private static float generateEBookChance(Enchantment enchantment, Random random)
	{
		var i = Math.max(0, Math.min(MIN_EBOOK_ACTIVATION_CHANCE.length, enchantment.getMaxLevel() - 1));

		var min = MIN_EBOOK_ACTIVATION_CHANCE[i];
		var max = MAX_EBOOK_ACTIVATION_CHANCE[i];

		var r = random.nextBetween(0, 1000000)/1000000f;
		r = 1f - (float)Math.sin(Math.acos(r));

		var chance = min + r * (max - min);
		return chance;
	}

	private static void configureBuiltInEnchantments()
	{
		// Common enchantments
		configureEnchantment("minecraft:efficiency", 100f);
		configureEnchantment("minecraft:protection", 100f);
		configureEnchantment("minecraft:feather_falling", 100f);
		configureEnchantment("minecraft:sharpness", 100f);
		configureEnchantment("minecraft:fortune", 100f);
		configureEnchantment("minecraft:silk_touch", 100f);
		configureEnchantment("minecraft:unbreaking", 100f);

		configureEnchantment("minecraft:respiration", 50f);
		configureEnchantment("minecraft:aqua_affinity", 50f);
		configureEnchantment("minecraft:depth_strider", 50f);
		configureEnchantment("minecraft:power", 50f);

		configureEnchantment("minecraft:looting", 35f);
		configureEnchantment("minecraft:sweeping", 35f);
		configureEnchantment("minecraft:infinity", 35f);


		configureEnchantment("minecraft:fire_protection", 20f);
		configureEnchantment("minecraft:blast_protection", 20f);
		configureEnchantment("minecraft:projectile_protection", 20f);

		// Very situational
		configureEnchantment("minecraft:smite", 10f);
		configureEnchantment("minecraft:bane_of_arthropods", 10f);
		configureEnchantment("minecraft:thorns", 10f);
		configureEnchantment("minecraft:fire_aspect", 10f);
		configureEnchantment("minecraft:flame", 10f);
		configureEnchantment("minecraft:knockback", 10f);
		configureEnchantment("minecraft:punch", 10f);
		configureEnchantment("minecraft:loyalty", 10f);
		configureEnchantment("minecraft:impaling", 10f);
		configureEnchantment("minecraft:riptide", 10f);

		// Fishing Rod
		configureEnchantment("minecraft:luck_of_the_sea", 2f);
		configureEnchantment("minecraft:lure", 2f);

		// Rarest enchantments
		configureEnchantment("minecraft:mending", 1f);
		configureEnchantment("minecraft:vanishing_curse", 1f);
		configureEnchantment("minecraft:binding_curse", 1f);
		configureEnchantment("minecraft:swift_sneak", 1f);
		configureEnchantment("minecraft:frost_walker", 1f);
		configureEnchantment("minecraft:channeling", 1f);

	}

	private static final float CHEST_NEVER = -1f;
	private static final float CHEST_VILLAGER = 0.33f;
	private static final float CHEST_STANDARD = 0.5f;
	private static final float CHEST_NETHER = 0.67f;
	private static final float CHEST_NETHER_PORTAL = 0.75f;
	private static final float CHEST_STRONGHOLD_LIBRARY = 0.75f;
	private static final float CHEST_UNDERWATER_RUINS = 0.75f;
	private static final float CHEST_ANCIENT_CITY = 0.75f;
	private static final float CHEST_ALWAYS = 1f;

	private static void configureBuiltInLootTables()
	{
		// Village
		configureLootTableRollChance("minecraft:chests/village/village_armorer", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_butcher", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_cartographer", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_desert_house", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_fisher", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_fletcher", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_mason", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_plains_house", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_savanna_house", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_shepherd", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_snowy_house", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_taiga_house", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_tannery", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_temple", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_toolsmith", CHEST_VILLAGER);
		configureLootTableRollChance("minecraft:chests/village/village_weaponsmith", CHEST_VILLAGER);

		// ---- Overworld
		// Always
		configureLootTableRollChance("minecraft:chests/igloo_chest", CHEST_ALWAYS);
		configureLootTableRollChance("minecraft:chests/shipwreck_treasure", CHEST_ALWAYS);
		configureLootTableRollChance("minecraft:chests/buried_treasure", CHEST_ALWAYS);

		configureLootTableRollChance("minecraft:chests/underwater_ruin_big", CHEST_UNDERWATER_RUINS);
		configureLootTableRollChance("minecraft:chests/underwater_ruin_small", CHEST_UNDERWATER_RUINS);

		configureLootTableRollChance("minecraft:chests/ancient_city", CHEST_ANCIENT_CITY);
		configureLootTableRollChance("minecraft:chests/ancient_city_ice_box", CHEST_ANCIENT_CITY);

		configureLootTableRollChance("minecraft:chests/abandoned_mineshaft", CHEST_STANDARD);

		configureLootTableRollChance("minecraft:chests/desert_pyramid", CHEST_STANDARD);

		configureLootTableRollChance("minecraft:chests/jungle_temple", CHEST_STANDARD);
		configureLootTableRollChance("minecraft:chests/jungle_temple_dispenser", CHEST_STANDARD);

		configureLootTableRollChance("minecraft:chests/pillager_outpost", CHEST_STANDARD);

		configureLootTableRollChance("minecraft:chests/shipwreck_map", CHEST_STANDARD);
		configureLootTableRollChance("minecraft:chests/shipwreck_supply", CHEST_STANDARD);

		configureLootTableRollChance("minecraft:chests/simple_dungeon", CHEST_STANDARD);

		configureLootTableRollChance("minecraft:chests/stronghold_corridor", CHEST_STANDARD);
		configureLootTableRollChance("minecraft:chests/stronghold_crossing", CHEST_STANDARD);
		configureLootTableRollChance("minecraft:chests/stronghold_library", CHEST_STRONGHOLD_LIBRARY);

		configureLootTableRollChance("minecraft:chests/woodland_mansion", CHEST_STANDARD);

		configureLootTableRollChance("minecraft:chests/ruined_portal", CHEST_NETHER_PORTAL);

		// ---- Nether
		configureLootTableRollChance("minecraft:chests/bastion_treasure", CHEST_ALWAYS);
		configureLootTableRollChance("minecraft:chests/bastion_bridge", CHEST_NETHER);
		configureLootTableRollChance("minecraft:chests/bastion_hoglin_stable", CHEST_NETHER);
		configureLootTableRollChance("minecraft:chests/bastion_other", CHEST_NETHER);
		configureLootTableRollChance("minecraft:chests/nether_bridge", CHEST_NETHER);

		// ---- End
		configureLootTableRollChance("minecraft:chests/end_city_treasure", CHEST_STANDARD);

		// ---- Special
		configureLootTableRollChance("minecraft:chests/spawn_bonus_chest", CHEST_NEVER);
	}
}