package net.fabricmc.starbidou.ebook.mixin;

import net.fabricmc.starbidou.ebook.EnchantingLogic;
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.EnchantedBookItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.EnchantmentScreenHandler;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.*;

@Mixin(EnchantmentScreenHandler.class)
public abstract class EnchantMixin extends ScreenHandler {



    protected EnchantMixin(@Nullable ScreenHandlerType<?> type, int syncId) {
        super(type, syncId);
    }

    @Inject(method = "onButtonClick", at = @At(value = "INVOKE", target = "Lnet/minecraft/screen/ScreenHandlerContext;run(Ljava/util/function/BiConsumer;)V"), locals = LocalCapture.CAPTURE_FAILEXCEPTION, cancellable = true)
    private void injected(PlayerEntity player, int id, CallbackInfoReturnable<Boolean> cir, ItemStack itemStack, ItemStack itemStack2, int i)
    {
        var that = ((EnchantmentScreenHandler)(Object)this);
        var context = that.context;

        context.run((world, pos) -> {
            ItemStack itemStack3 = itemStack;
            List<EnchantmentLevelEntry> list = that.generateEnchantments(itemStack3, id, that.enchantmentPower[id]);
            EnchantingLogic.completeEnchantmentList(list, itemStack3, world, pos, that.random);

            if (!list.isEmpty()) {
                player.applyEnchantmentCosts(itemStack3, i);
                boolean bl = itemStack3.isOf(Items.BOOK);
                if (bl) {
                    itemStack3 = new ItemStack(Items.ENCHANTED_BOOK);
                    NbtCompound nbtCompound = itemStack.getNbt();
                    if (nbtCompound != null) {
                        itemStack3.setNbt(nbtCompound.copy());
                    }
                    that.inventory.setStack(0, itemStack3);
                }
                for (int k = 0; k < list.size(); ++k) {
                    EnchantmentLevelEntry enchantmentLevelEntry = list.get(k);
                    if (bl) {
                        EnchantedBookItem.addEnchantment(itemStack3, enchantmentLevelEntry);
                        continue;
                    }
                    itemStack3.addEnchantment(enchantmentLevelEntry.enchantment, enchantmentLevelEntry.level);
                }

                if (!player.getAbilities().creativeMode) {
                    itemStack2.decrement(i);
                    if (itemStack2.isEmpty()) {
                        that.inventory.setStack(1, ItemStack.EMPTY);
                    }
                }
                player.incrementStat(Stats.ENCHANT_ITEM);
                if (player instanceof ServerPlayerEntity) {
                    Criteria.ENCHANTED_ITEM.trigger((ServerPlayerEntity)player, itemStack3, i);
                }
                that.inventory.markDirty();
                that.seed.set(player.getEnchantmentTableSeed());
                that.onContentChanged(that.inventory);
                world.playSound(null, (BlockPos)pos, SoundEvents.BLOCK_ENCHANTMENT_TABLE_USE, SoundCategory.BLOCKS, 1.0f, world.random.nextFloat() * 0.1f + 0.9f);
            }
        });

        cir.setReturnValue(true);
    }



}
