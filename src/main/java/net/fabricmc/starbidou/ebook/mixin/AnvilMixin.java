package net.fabricmc.starbidou.ebook.mixin;

import net.fabricmc.starbidou.ebook.EBookMod;
import net.fabricmc.starbidou.ebook.EBookUtils;
import net.fabricmc.starbidou.ebook.EnchantingLogic;
import net.fabricmc.starbidou.ebook.EnchantmentRoll;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.*;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.List;

@Mixin(AnvilScreenHandler.class)
public abstract class AnvilMixin extends ForgingScreenHandler {


    @Shadow @Final private Property levelCost;

    @Shadow private int repairItemUsage;

    public AnvilMixin(@Nullable ScreenHandlerType<?> type, int syncId, PlayerInventory playerInventory, ScreenHandlerContext context) {
        super(type, syncId, playerInventory, context);
    }

    @Inject(method = "updateResult", at = @At("HEAD"), locals = LocalCapture.CAPTURE_FAILEXCEPTION, cancellable = true)
    private void injected(CallbackInfo ci)
    {
        EBookMod.LOGGER.info("Working Mixin");
        var that = ((AnvilScreenHandler)(Object)this);

        ItemStack leftItem = this.input.getStack(0);
        List<EnchantmentRoll> leftRolls = EBookUtils.GetEnchantmentRolls(leftItem);


        EBookMod.LOGGER.info("LEFT: " + leftRolls.size());
        if(leftRolls.size() == 0) return;

        ItemStack rightItem = this.input.getStack(1);
        List<EnchantmentRoll> rightRolls = EBookUtils.GetEnchantmentRolls(rightItem);

        EBookMod.LOGGER.info("RIGHT: " + rightRolls.size());
        if(rightRolls.size() == 0)
        {
            this.repairItemUsage = 0;
            this.levelCost.set(0);
            this.output.setStack(0, ItemStack.EMPTY);
            this.sendContentUpdates();

            ci.cancel();
            return;
        }

        List<EnchantmentRoll> mergedRolls = EBookUtils.MergeRolls(leftRolls, rightRolls);
        EBookMod.LOGGER.info("MERGED: " + mergedRolls.size());

        ItemStack mergedItem = EBookUtils.generateEBook(mergedRolls.toArray(new EnchantmentRoll[0]));

        this.repairItemUsage = 0;
        this.levelCost.set(1);
        this.output.setStack(0, mergedItem);
        this.sendContentUpdates();

        ci.cancel();
    }

}
