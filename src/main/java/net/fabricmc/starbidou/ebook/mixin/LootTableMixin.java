package net.fabricmc.starbidou.ebook.mixin;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.fabricmc.starbidou.ebook.EnchantingLogic;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextTypes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(LootTable.class)
public abstract class LootTableMixin {

    @Inject(method="generateLoot(Lnet/minecraft/loot/context/LootContext;)Lit/unimi/dsi/fastutil/objects/ObjectArrayList;", at= @At("TAIL"), locals = LocalCapture.CAPTURE_FAILEXCEPTION)
    private void inject(LootContext context, CallbackInfoReturnable<ObjectArrayList<ItemStack>> cir, ObjectArrayList objectArrayList)
    {
//        var lootTable = ((LootTable)(Object)this);
//        if( lootTable.getType() == LootContextTypes.CHEST)
//        {
//            var random = context.getRandom();
//            var itemStack = EnchantingLogic.generateItemStack(random);
//            if( !itemStack.isEmpty())
//            {
//                objectArrayList.add(itemStack);
//            }
//        }
    }
}
