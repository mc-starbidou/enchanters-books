package net.fabricmc.starbidou.ebook;

import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTable;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtHelper;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EnchantingLogic {
    private static final Identifier ENCHANTED_BOOK_IDENTIFIER = new Identifier("minecraft", "enchanted_book");

    public static void completeEnchantmentList(List<EnchantmentLevelEntry> list, ItemStack itemToEnchant, World world, BlockPos pos, Random random)
    {
        EBookMod.LOGGER.info(pos.toShortString());

        var blockEntityBelow = world.getBlockEntity(pos.add(0, -1, 0));

        if(blockEntityBelow instanceof LootableContainerBlockEntity)
        {
            var container = (LootableContainerBlockEntity)blockEntityBelow;
            var size = container.size();

            List<String> tagList = new ArrayList<>();

            if( size == 27 )
            {
                for(int i = 0; i < size; ++i)
                {
                    tagList.clear();
                    var slotItemStack = container.getStack(i);
                    var item = slotItemStack.getItem();

                    if( Registries.ITEM.getId(item).compareTo(ENCHANTED_BOOK_IDENTIFIER) == 0)
                    {
                        var rollList = EBookUtils.GetEnchantmentRolls(slotItemStack);
                        for(var roll : rollList)
                        {
                            var identifier = new Identifier(roll.enchantmentIdentifier);
                            if(Registries.ENCHANTMENT.containsId(identifier))
                            {
                                if( random.nextFloat() <= roll.chance)
                                {
                                    MergeEnchantment(list, Registries.ENCHANTMENT.get(identifier), itemToEnchant);
                                }
                            }
                        }
                    }
                }
            }

            EBookMod.LOGGER.info(String.format("Container size: %d", size));
        }
    }

    private static void MergeEnchantment(List<EnchantmentLevelEntry> list, Enchantment enchantment, ItemStack itemStack)
    {
        if(!enchantment.isAcceptableItem(itemStack)) return;

        // All previous enchantments must be compatible
        int duplicate = -1;
        for(int i = 0; i < list.size(); ++i)
        {
            var entry = list.get(i);

            if(entry.enchantment == enchantment)
            {
                duplicate = i;
            }
            else if(!entry.enchantment.canCombine(enchantment))
            {
                return;
            }
        }

        if(duplicate >= 0)
        {
            // Increase level when already in list
            int level = list.get(duplicate).level + 1;

            if( level <= enchantment.getMaxLevel())
            {
                list.set(duplicate, new EnchantmentLevelEntry(enchantment, level));
            }
        }
        else
        {
            list.add(new EnchantmentLevelEntry(enchantment, 1));
        }
    }


}
