package net.fabricmc.starbidou.ebook;

import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.loot.function.LootFunctionType;
import net.minecraft.loot.function.LootFunctionTypes;

public class EBookLootFunction implements LootFunction {
    @Override
    public LootFunctionType getType() {
        return LootFunctionTypes.ENCHANT_RANDOMLY;
    }

    @Override
    public ItemStack apply(ItemStack itemStack, LootContext lootContext) {
        return EBookMod.generateEBook(lootContext.getRandom());
    }
}