package net.fabricmc.starbidou.ebook;

import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.provider.number.LootNumberProvider;
import net.minecraft.loot.provider.number.LootNumberProviderType;
import net.minecraft.loot.provider.number.LootNumberProviderTypes;
import net.minecraft.util.Identifier;

public class EBookChanceProvider implements LootNumberProvider {

    private Identifier lootTableIdentifier;

    public EBookChanceProvider(Identifier lootTableIdentifier) {
        this.lootTableIdentifier = lootTableIdentifier;
    }

    @Override
    public float nextFloat(LootContext context)
    {
        var random = context.getRandom();
        return random.nextBetween(1, 1000000)/1000000f <= EBookMod.getLootTableRollChance(lootTableIdentifier) ? 1f : 0f;
    }

    @Override
    public LootNumberProviderType getType() {
        return LootNumberProviderTypes.CONSTANT;
    }
}
